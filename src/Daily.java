public class Daily extends Appointment{
    Daily(String descriptionArg, int monthArg, int dayArg, int yearArg ){
        super(descriptionArg, monthArg, dayArg, yearArg);
    }
    boolean occursOn(int yearArg, int monthArg, int dayArg){
        return true;//it would always return true becuse it happens daily right
    }
    int getMonth(){return date.get(0);}
    int getDay(){return date.get(1);}

    int getYear(){return date.get(2);}
    String getDescription(){return description;}

}
