import java.time.Month;
import java.util.Scanner;
import java.util.ArrayList;
public class p94AppointmentDriver {

    public static void main(String[] args) {
        String input;
        Scanner myObj = new Scanner(System.in);
        ArrayList<Monthly> MonthDatabase = new ArrayList<Monthly>();
        ArrayList<OneTime> oneTDatabase = new ArrayList<OneTime>();
        ArrayList<Daily> dailyDatabase = new ArrayList<Daily>();

        do {
            System.out.println("Select an option");
            System.out.println("A for add appointment");
            System.out.println("C for checking");
            System.out.println("Q for quit");
            input = myObj.nextLine();
            if(input.equals("a")){
                String typeInput;
                System.out.println("Enter the type (O - onetime, D - Daily, M - monthly)");
                typeInput = myObj.nextLine();

                ArrayList<Integer> date = new ArrayList<Integer>();
                int yearInput, dayInput, monthInput;
                System.out.println("Enter the month");
                monthInput = myObj.nextInt();
                date.add(monthInput);

                System.out.println("Enter the day");
                dayInput = myObj.nextInt();
                date.add(dayInput);

                System.out.println("Enter the year");
                yearInput = myObj.nextInt();
                date.add(yearInput);

                String descriptionInput;
                System.out.println("Enter the description");
                descriptionInput = myObj.nextLine();



                if(typeInput.equals("O")) {
                    OneTime oneTobject = new OneTime(descriptionInput, date.get(0), date.get(1), date.get(2));
                    oneTDatabase.add(oneTobject);
                }
                else if (typeInput.equals("D")){
                    Daily dailyObject = new Daily(descriptionInput,date.get(0),date.get(1),date.get(2));
                    dailyDatabase.add(dailyObject);
                }
                else if (typeInput.equals("M")){
                    Monthly monthObject = new Monthly(descriptionInput, date.get(0),date.get(1),date.get(2));
                    MonthDatabase.add(monthObject);
                }
            }
            else if (input.equals("c")){
                int yearInput;
                System.out.println("Enter the year");
                yearInput = myObj.nextInt();
                for(int i = 0; i < dailyDatabase.size();i++){
                    if(dailyDatabase.get(i).getYear() < yearInput){
                        System.out.println("The daily event happens on this day ");
                        System.out.println(dailyDatabase.get(i).getMonth() + dailyDatabase.get(i).getDay());
                        System.out.println(dailyDatabase.get(i).getDescription());
                    }
                }//for loop

                int dayInput;
                System.out.println("Enter the day");
                dayInput = myObj.nextInt();
                for(int i = 0; i< MonthDatabase.size(); i++){
                    if(MonthDatabase.get(i).getDay()==dayInput){
                        //if it is monthly we just need to have the same day
                        System.out.println( MonthDatabase.get(i).getMonth() + MonthDatabase.get(i).getDay() + MonthDatabase.get(i).getYear() );
                        System.out.println(MonthDatabase.get(i).getDescription() );

                    }
                }
                int monthInput;
                System.out.println("Enter the month");
                monthInput = myObj.nextInt();
                for(int i = 0; i<oneTDatabase.size();i++){
                    if(oneTDatabase.get(i).getMonth()==monthInput && oneTDatabase.get(i).getDay()==dayInput && oneTDatabase.get(i).getYear()==yearInput){
                        //checks if the date input equals to the one in the "server"
                        System.out.println("These are the one time events that happen on the day");
                        System.out.println( oneTDatabase.get(i).getMonth() + oneTDatabase.get(i).getDay() + oneTDatabase.get(i).getYear() );
                        System.out.println(oneTDatabase.get(i).getDescription());
                    }
                }

            }
            else if (input.equals("q")) {
                break;
            }
            else{System.out.println("Invalid input please either enter an uppercase o or an uppercase d or uppercase m");}
        }while(!input.equals("q"));//loop ending bracket

    }
}
