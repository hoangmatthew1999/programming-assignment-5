import java.awt.Rectangle;
public class extendedRect extends Rectangle {
    extendedRect(int heightArg, int widthArg){
        super(widthArg,heightArg);
    }
    int getPerimeter(int height,int width){
        int perimeter = height + height + width + width;
        return perimeter;
    }
    int getArea(int height, int width){
        return height * width;
    }

    public static void main(String[] args) {
        extendedRect object = new extendedRect(12,12);
    }
}
