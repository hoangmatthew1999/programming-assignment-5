public class OneTime extends Appointment {
    OneTime(String descriptionArg, int monthArg, int dayArg, int yearArg){
        super(descriptionArg, monthArg, dayArg, yearArg);
    }
    Boolean occursOn(int yearArg, int monthArg, int dayArg){
        if(yearArg != date.get(2)){return false;}
        if(dayArg != date.get(1)){return false;}
        if(monthArg != date.get(0)){return false;}
        return true;
    }
    int getMonth(){return date.get(0);}
    int getDay(){return date.get(1);}

    int getYear(){return date.get(2);}
    String getDescription(){return description;}
}
