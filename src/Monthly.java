public class Monthly extends Appointment {
    Monthly(String descriptionArg, int monthArg, int dayArg, int yearArg)
    {super(descriptionArg, monthArg, dayArg, yearArg);}
    int getMonth(){return date.get(0);}
    int getDay(){return date.get(1);}

    int getYear(){return date.get(2);}
    String getDescription(){return description;}


}
